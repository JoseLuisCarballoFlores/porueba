import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JOptionPane;

import java.util.GregorianCalendar;
import java.util.Date;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;

public class Hilo implements Runnable{
    
	 private String ahoraMismo;	
	 private DateTimeFormatter hora = DateTimeFormatter.ofPattern("HH:mm");
	
	public void run() {		
		
		while(true){  //bucle infinito
		ahoraMismo=leerFichero();		
		try {
			if(ahoraMismo.equals(hora.format(LocalDateTime.now()))) {         //Si la hora coincide, mando el email a través del hilo
				Funciones.mandarCorreoAutomatico();													
				}
			Thread.sleep(60000);                //Le duermo para que no este todo el rato comparandose con la hora
		} catch (InterruptedException e) {
			JOptionPane.showMessageDialog(null,"Error De Interrupción", "ERROR",0);
			
		}	   		 		   	
		}
	}	
	public String leerFichero() {		
		File f3 = new File("ConfiguracionEmail.txt");	 //Leo el fichero para coger la hora de envío y compararla con la hora del ordenador
		try {
			FileReader	fr = new FileReader(f3);
			BufferedReader br = new BufferedReader(fr);
			String linea;
			String parteLinea[];			
			linea=br.readLine();			
			parteLinea=linea.split(";");
			br.close();
			return parteLinea[2];			
		} catch (FileNotFoundException e) {
			JOptionPane.showMessageDialog(null, "ERROR EN FICHEROS","ERROR" , 0);
			return e.getMessage();
			
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "ERROR EN FICHEROS","ERROR" , 0);
			return e.getMessage();
		}		
	}
}
