import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JProgressBar;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.Timer;
import javax.swing.text.MaskFormatter;
import javax.swing.text.NumberFormatter;

import java.awt.Font;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;
import java.text.NumberFormat;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JComboBox;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.event.ActionEvent;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.jsoup.select.Evaluator.IsEmpty;

import javax.swing.JTextPane;
import java.awt.Cursor;
import javax.swing.JCheckBox;

import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.ImageIcon;
import javax.swing.DebugGraphics;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Calendar;
import javax.swing.JTable;
import javax.swing.JToggleButton;
import javax.swing.JSeparator;
import javax.swing.JFormattedTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.SpinnerListModel;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class ProyectoFinalJoseLuis {
	private JFrame frmBreakingnews;
	private JTextField txtUsuario;
	private JPasswordField txtPassword;
	private JPasswordField txtRegistroPassword;
	private JTextField txtRegistroUsuario;
	private JTextField txtRegistroEmail;
	Timer timer; // Inicializar Timer
	int i = 0; // Inicializo i para la barra de progreso y le doy valor 0
	// -------------------------------Declaracón de
	// paneles-----------------------------------------------------------
	// PANEL TÍTULO!!
	JPanel panelTitulo1 = new JPanel();
	// PANEL DE LOGIN !!
	JPanel panelLogin2 = new JPanel();
	// REGISTRO !!
	JPanel panelRegistro3 = new JPanel();
	// ELEGIR NOTICIAS!!
	JPanel panelMostrarPreferencias5 = new JPanel();
	// MOSTRAR NOTICIAS !!
	JPanel panelElegirNoticias4 = new JPanel();
	// ADMINISTRADOR USUARIOS !!
	JPanel panelAdministradorUsuarios6 = new JPanel();
	// ADMINISTRADOR NOTICIAS !!
	JPanel panelAdministradorTesting7 = new JPanel();
	// ---------------Declaración de
	// textos-------------------------------------------------
	// MOSTRAR NOTICIAS 1
	JTextPane txtPane1 = new JTextPane();
	// MOSTRAR NOTICIAS 2
	JTextPane txtPane2 = new JTextPane();
	// MOSTRAR NOTICIAS 3
	JTextPane txtPane3 = new JTextPane();

	// Label AnimeMisionTokyo
	JLabel lblAnimePeriódico1 = new JLabel("MisionTokyo");
	// Label anime republica
	JLabel lblAnimePeriódico2 = new JLabel("Rep\u00FAblica");
	// Label anime ramen para 2
	JLabel lblAnimePeriódico3 = new JLabel("Ramen Para 2");
	// label deporte
	JLabel lblDeportePeriodico1 = new JLabel("El Mundo");
	// label deporte
	JLabel lblDeportePeriodico3 = new JLabel("Marca");
	// label deporte
	JLabel lblDeportePeriodico2 = new JLabel("El Pa\u00EDs");
	// Label arqueología
	JLabel lblArqueologíaPeriodico3 = new JLabel("20 Minutos");
	// Label arqueología
	JLabel lblArqueologíaPeriodico2 = new JLabel("ABC");
	// Label arqueología
	JLabel lblArqueologíaPeriodico1 = new JLabel("El Pa\u00EDs");
	// label mostrar preferencias deporte
	JLabel lblMostrarPreferenciasDeporte = new JLabel("Deporte");
	// label mostrar preferencias arqueologia
	JLabel lblMostrarPreferenciaArqueologia = new JLabel("Arqueolog\u00EDa");
	// label mostrar preferencias anime
	JLabel lblMostrarPreferenciaAnime = new JLabel("Anime");
	// label usuario LOGIN
	JLabel labelUsuario = new JLabel("Usuario");
	// label password LOGIN
	JLabel labelPassword = new JLabel("Password");
	// label titulo LOGIN
	JLabel labelLogin = new JLabel("Iniciar Sesi\u00F3n", SwingConstants.CENTER);
	// REGISTRO
	JLabel labelRegistro = new JLabel("Registrarse", SwingConstants.CENTER);
	// REGISTRO
	JLabel labelUsuarioRegistro = new JLabel("Usuario");
	// REGISTRO
	JLabel labelContraseñaRegistro = new JLabel("Password");
	// REGISTRO
	JLabel labelEmailRegistro = new JLabel("E-mail");
	// Labels Admin Usuarios
	JLabel lblAdminDep = new JLabel("Deporte");
	JLabel lblAdminArqu = new JLabel("Arqueolog\u00EDa");
	JLabel lblAdminAni = new JLabel("Anime");
	// Label de la imagen
	JLabel lblImagen = new JLabel("");

	// ------------------------declaración de
	// checkboxs-----------------------------------------------------------
	// checkbox deportes
	JCheckBox chckbxDeporteElMundo = new JCheckBox("El Mundo");
	// checkbox deportes
	JCheckBox chckbxDeporteElPaís = new JCheckBox("El Pa\u00EDs");
	// checkbox deportes
	JCheckBox chckbxDeporteMarca = new JCheckBox("Marca");
	// checkbox arqueologia
	JCheckBox chckbxArqueologíaElPaís = new JCheckBox("El Pa\u00EDs");
	// checkbox arqueologia
	JCheckBox chckbxArqueologíaABC = new JCheckBox("ABC");
	// checkbox arqueologia
	JCheckBox chckbxArqueología20Minutos = new JCheckBox("20 Minutos");
	// checkbox anime
	JCheckBox chckbxAnimeMisionTokyo = new JCheckBox("MisionTokyo");
	// checkbox anime
	JCheckBox chckbxAnimeRepublica = new JCheckBox("Rep\u00FAblica.pe");
	// checkbox anime
	JCheckBox chckbxAnimeRamenPara2 = new JCheckBox("Ramen Para 2");
	// Elegir Usuario1
	JRadioButton ElegirUsuario1 = new JRadioButton("");
	// Elegir Usuario2
	JRadioButton ElegirUsuario2 = new JRadioButton("");
	// Elegir Usuario3
	JRadioButton ElegirUsuario3 = new JRadioButton("");
	// chbox ElMundoDep
	JCheckBox bxElMundoDep = new JCheckBox("El Mundo");
	// chbox ElPaisDep
	JCheckBox bxElPaisDep = new JCheckBox("El Pa\u00EDs");
	// chbox MarcaDep
	JCheckBox bxMarcaDep = new JCheckBox("Marca");
	JCheckBox bxElPaisArq = new JCheckBox("El Pa\u00EDs");
	JCheckBox bxABCArq = new JCheckBox("ABC");
	JCheckBox bx20MinArq = new JCheckBox("20Minutos");
	JCheckBox bxMisionTokyoAni = new JCheckBox("MisionTokyo");
	JCheckBox bxRepublicaAni = new JCheckBox("Rep\u00FAblica.pe");
	JCheckBox bxRamenPara2 = new JCheckBox("Ramen Para 2");

	// ----------------------------Declaración de
	// botones------------------------------------------------------------
	// BOTÓN START "INICIO"
	JButton btonStart = new JButton("START");
	// BOTÓN IR A LA PÁGINA DE REGISTRO "LOGIN"
	JButton buttonRegistro = new JButton("Registrarse");
	// BOTÓN CONFIRMAR LOGIN PARA IR A LA APP "LOGIN"
	JButton buttonLogin = new JButton("Confirmar");
	// BOTÓN VOLVER AL LOGIN DESDE EL "REGISTRO"
	JButton buttonVolverLogin = new JButton("Login");
	// BOTÓN SALIR DESDE "ELEGIR NOTICIAS"
	JButton buttonSalirElegirNoticias = new JButton("Salir");
	// BOTÓN SALIR DE MOSTRAR NOTICIAS, PARA VOLVER AL LOGIN "MOSTRAR NOTICIAS"
	JButton buttonCerrarSesión = new JButton("Salir");
	// BOTÓN SALIR DE ADMINISTRADOR, VUELVE AL INICIO DE SESIÓN "ADMINISTRADOR
	// USUARIOS"
	JButton buttonSalirDeAdmin = new JButton("Salir");
	// BOTÓN SIGUIENTE PARA IR A ADMINISTRADOR DE NOTICIAS, DESDE "ADMINISTRADOR
	// USUARIOS"
	JButton buttonSiguienteNoticiasAdmin = new JButton("Noticias");
	// BOTÓN SALIR QUE LLEVA AL INICIO DE SESIÓN DESDE "ADMINISTRADOR NOTICIAS"
	JButton buttonSalirDeAdminNoticias = new JButton("Salir");
	// BOTÓN QUE LLEVA AL ADMIN USUARIOS, DESDE "ADMINISTRADOR DE NOTICIAS"
	JButton buttonVolverAdminUsuarios = new JButton("Usuarios");
	// BOTÓN NOTICIAS DE DEPORTES
	JButton btnNoticiasDeporte = new JButton("Deporte");
	// BOTÓN NOTICIAS DE VIDEOJUEGOS
	JButton btnNoticiasAnime = new JButton("Anime");
	// BOTÓN NOTICIAS DE GEOGRAFÍA
	JButton btnNoticiasGeografía = new JButton("Arqueolog\u00EDa");
	// Botón para enviar noticias
	JButton btnEnviarNoticias = new JButton("Enviar Noticias");
	// botones de salir de la app
	JButton btnSalir1 = new JButton("");
	JButton btnSalir2 = new JButton("");
	JButton btnSalir3 = new JButton("");
	JButton btnSalir4 = new JButton("");
	JButton btnSalir5 = new JButton("");
	JButton btnSalir6 = new JButton("");
	JButton btnSalir7 = new JButton("");
	JButton btnCambioHora = new JButton("Cambiar hora");
	// Botón guardar preferencias de los usuarios
	JButton btnGuardarAdminUsu = new JButton("Guardar");
	// --------------------------------------------------Creación de
	// ficheros------------------------------------------------------
	File f1 = new File("Noticias.txt");
	File f2 = new File("Usuarios.txt");
	File f3 = new File("ConfiguracionEmail.txt");
	// Creación del objeto usuario
	Usuario usuario = new Usuario();
	private final JTextField txtHora = new JTextField();
	private final JTextField txtMin = new JTextField();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ProyectoFinalJoseLuis window = new ProyectoFinalJoseLuis();
					window.frmBreakingnews.setVisible(true);
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, "ERROR EN EL PROGRAMA","ERROR" , 0);
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ProyectoFinalJoseLuis() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		// Frame
		frmBreakingnews = new JFrame();
		frmBreakingnews.setTitle("BreakingNews");
		frmBreakingnews.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		frmBreakingnews.setIconImage(
				Toolkit.getDefaultToolkit().getImage(ProyectoFinalJoseLuis.class.getResource("/Im\u00E1genes/L.png")));
		frmBreakingnews.setBounds(100, 100, 792, 548);
		frmBreakingnews.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmBreakingnews.getContentPane().setLayout(null);
		frmBreakingnews.setLocationRelativeTo(null);
		frmBreakingnews.setResizable(false);
		panelMostrarPreferencias5.setVisible(false);
		panelAdministradorUsuarios6.setVisible(false);

		// Ejecuto el hilo fuera del main para que tarde un poco en dar el aviso
		Hilo hilo1 = new Hilo();
		Thread hiloH = new Thread(hilo1);
		// Si no hay conexión a internet te saca de la aplicación, pero si hay conexión
		// ejecuta el programa
		if (!Funciones.comprobarConexion()) {
			JOptionPane.showMessageDialog(null, "No tienes conexión a internet", "Comprueba tu conexión", 0);
			System.exit(0);
		} else {
			hiloH.start();
		}
				
        panelRegistro3.setVisible(false);
    panelLogin2.setVisible(false);
    txtMin.addKeyListener(new KeyAdapter() { // Para que solo me escriba numeros
        @Override
        public void keyTyped(KeyEvent e) {
            char caracter = e.getKeyChar();
            if ((caracter < '0' || caracter > '9') && (caracter != '\b')) {

                JOptionPane.showMessageDialog(null, "Sólo se admiten numeros", "Sólo números", 2);
                txtHora.setText("");
                e.consume();
            }
        }
    });
    txtMin.setBounds(507, 465, 30, 20);
    txtMin.setColumns(10);
    txtHora.addKeyListener(new KeyAdapter() { // Para que solo me escriba numeros
        @Override
        public void keyTyped(KeyEvent e) {
            char caracter = e.getKeyChar();
            if ((caracter < '0' || caracter > '9') && (caracter != '\b')) {
            
                JOptionPane.showMessageDialog(null, "Sólo se admiten numeros", "Sólo números", 2);
                txtMin.setText("");
                e.consume();
            }

        }
    });
    txtHora.setBounds(469, 465, 30, 20);
    txtHora.setColumns(10);
    panelAdministradorTesting7.setVisible(false);
    
            // ADMINISTRADOR NOTICIAS
            panelAdministradorTesting7.setBounds(0, 0, 776, 509);
            frmBreakingnews.getContentPane().add(panelAdministradorTesting7);
            panelAdministradorTesting7.setLayout(null);
            // ADMINISTRADOR NOTICIAS
            JLabel labelAdminNoticias = new JLabel("Administrador Testing", SwingConstants.CENTER);
            labelAdminNoticias.setForeground(Color.BLUE);
            labelAdminNoticias.setFont(new Font("Snap ITC", Font.PLAIN, 35));
            labelAdminNoticias.setBounds(157, 11, 469, 105);
            panelAdministradorTesting7.add(labelAdminNoticias);
            
                    // BOTÓN SALIR QUE LLEVA AL INICIO DE SESIÓN DESDE "ADMINISTRADOR NOTICIAS"
                    buttonSalirDeAdminNoticias.setFont(new Font("Tahoma", Font.PLAIN, 16));
                    buttonSalirDeAdminNoticias.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent e) {
                            txtUsuario.setText("");
                            txtPassword.setText("");
                            panelLogin2.setVisible(true);
                            panelAdministradorTesting7.setVisible(false);
                        }
                    });
                    buttonSalirDeAdminNoticias.setBounds(10, 448, 121, 50);
                    panelAdministradorTesting7.add(buttonSalirDeAdminNoticias);
                    
                            // BOTÓN QUE LLEVA AL ADMIN USUARIOS, DESDE "ADMINISTRADOR DE NOTICIAS"
                            buttonVolverAdminUsuarios.setFont(new Font("Tahoma", Font.PLAIN, 16));
                            buttonVolverAdminUsuarios.addActionListener(new ActionListener() {
                                public void actionPerformed(ActionEvent e) {
                                    panelAdministradorUsuarios6.setVisible(true);
                                    panelAdministradorTesting7.setVisible(false);
                                }
                            });
                            buttonVolverAdminUsuarios.setBounds(144, 448, 121, 50);
                            panelAdministradorTesting7.add(buttonVolverAdminUsuarios);
                            
                                    JLabel lblPeriodico1 = new JLabel("");
                                    lblPeriodico1.setFont(new Font("Snap ITC", Font.PLAIN, 16));
                                    lblPeriodico1.setBounds(36, 219, 142, 50);
panelAdministradorTesting7.add(lblPeriodico1);

    JLabel lblPeriodico2 = new JLabel("");
    lblPeriodico2.setFont(new Font("Snap ITC", Font.PLAIN, 16));
    lblPeriodico2.setBounds(36, 294, 142, 50);
    panelAdministradorTesting7.add(lblPeriodico2);
    
            JLabel lblPeriodico3 = new JLabel("");
            lblPeriodico3.setFont(new Font("Snap ITC", Font.PLAIN, 16));
            lblPeriodico3.setBounds(36, 370, 142, 50);
            panelAdministradorTesting7.add(lblPeriodico3);
            btnNoticiasDeporte
                    .setIcon(new ImageIcon(ProyectoFinalJoseLuis.class.getResource("/Im\u00E1genes/deporte.png")));
            // BOTÓN NOTICIAS DE DEPORTES
            btnNoticiasDeporte.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    txtPane1.setText(Funciones.deporte1());
                    txtPane2.setText(Funciones.deporte2());
                    txtPane3.setText(Funciones.deporte3());
                    lblPeriodico1.setText("El Mundo");
                    lblPeriodico2.setText("El País");
                    lblPeriodico3.setText("Marca");
                }
            });
            btnNoticiasDeporte.setFont(new Font("Tahoma", Font.PLAIN, 13));
            btnNoticiasDeporte.setBounds(10, 125, 147, 40);
            panelAdministradorTesting7.add(btnNoticiasDeporte);
            btnNoticiasGeografía
                    .setIcon(new ImageIcon(ProyectoFinalJoseLuis.class.getResource("/Im\u00E1genes/hueso.png")));
            // BOTÓN NOTICIAS DE ARQUEOLOGÍA
            btnNoticiasGeografía.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    txtPane1.setText(Funciones.arqueologia1());
                    txtPane2.setText(Funciones.arqueologia2());
                    txtPane3.setText(Funciones.arqueologia3());
                    lblPeriodico1.setText("El País");
                    lblPeriodico2.setText("ABC");
                    lblPeriodico3.setText("20 minutos");
                }
            });
            btnNoticiasGeografía.setFont(new Font("Tahoma", Font.PLAIN, 13));
            btnNoticiasGeografía.setBounds(311, 125, 147, 40);
            panelAdministradorTesting7.add(btnNoticiasGeografía);
            btnNoticiasAnime.setIcon(new ImageIcon(ProyectoFinalJoseLuis.class.getResource("/Im\u00E1genes/porpcorn.png")));
            // BOTÓN NOTICIAS DE ANIME
            btnNoticiasAnime.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    txtPane1.setText(Funciones.anime1());
                    txtPane2.setText(Funciones.anime2());
                    txtPane3.setText(Funciones.anime3());
                    lblPeriodico1.setText("MisionTokyo");
                    lblPeriodico2.setText("Republica.pe");
                    lblPeriodico3.setText("Ramen Para 2");
                }
            });
            btnNoticiasAnime.setFont(new Font("Tahoma", Font.PLAIN, 13));
            btnNoticiasAnime.setBounds(624, 125, 142, 40);
            panelAdministradorTesting7.add(btnNoticiasAnime);
            // MOSTRAR NOTICIAS 1
            txtPane1.setEditable(false);
            txtPane1.setBounds(219, 219, 500, 50);
            panelAdministradorTesting7.add(txtPane1);
            // MOSTRAR NOTICIAS 2
            txtPane2.setEditable(false);
            txtPane2.setBounds(219, 294, 500, 50);
            panelAdministradorTesting7.add(txtPane2);
            // MOSTRAR NOTICIAS 3
            txtPane3.setEditable(false);
            txtPane3.setBounds(219, 370, 500, 50);
            panelAdministradorTesting7.add(txtPane3);
            // Botón para enviar noticias
            btnEnviarNoticias.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    Funciones.mandarCorreo();
                }
            });
            btnEnviarNoticias.setFont(new Font("Tahoma", Font.PLAIN, 16));
            btnEnviarNoticias.setBounds(645, 448, 121, 50);
            panelAdministradorTesting7.add(btnEnviarNoticias);
            btnSalir7.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    int resp = JOptionPane.showConfirmDialog(null, "Seguro que desea salir?");
                    if (JOptionPane.OK_OPTION == resp) {
                        System.exit(0);
                    }
                }
            });
            btnSalir7.setIcon(new ImageIcon(ProyectoFinalJoseLuis.class.getResource("/Im\u00E1genes/salir.png")));
            btnSalir7.setBounds(10, 11, 76, 50);
            panelAdministradorTesting7.add(btnSalir7);
            btnCambioHora.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    CambiarHora();
                }
            });
            btnCambioHora.setFont(new Font("Tahoma", Font.PLAIN, 14));
            btnCambioHora.setBounds(337, 450, 121, 50);
            panelAdministradorTesting7.add(btnCambioHora);
            
                    panelAdministradorTesting7.add(txtHora);
                    
                            panelAdministradorTesting7.add(txtMin);
								// LOGIN
								panelLogin2.setBounds(0, 0, 776, 509);
								frmBreakingnews.getContentPane().add(panelLogin2);
								panelLogin2.setLayout(null);
								labelUsuario.setFont(new Font("Snap ITC", Font.PLAIN, 20));
								labelUsuario.setBounds(170, 205, 89, 35);
								panelLogin2.add(labelUsuario);
								labelPassword.setFont(new Font("Snap ITC", Font.PLAIN, 20));
								labelPassword.setBounds(150, 306, 109, 35);
								panelLogin2.add(labelPassword);
								labelLogin.setForeground(Color.BLUE);
								labelLogin.setFont(new Font("Snap ITC", Font.PLAIN, 35));
								labelLogin.setBounds(219, 11, 320, 105);
								panelLogin2.add(labelLogin);
								txtUsuario = new JTextField();
								txtUsuario.setBounds(269, 208, 235, 37);
								panelLogin2.add(txtUsuario);
								txtUsuario.setColumns(10);
								txtPassword = new JPasswordField();
								txtPassword.setBounds(269, 309, 235, 36);
								panelLogin2.add(txtPassword);
								buttonRegistro.setFont(new Font("Tahoma", Font.PLAIN, 16));
								// BOTÓN DEL LOGIN PARA IR A LA VENTANA DE REGISTRO
								buttonRegistro.addActionListener(new ActionListener() {
									public void actionPerformed(ActionEvent e) {
										panelRegistro3.setVisible(true);
										panelLogin2.setVisible(false);
										txtUsuario.setText("");
										txtPassword.setText("");
									}
								});
								buttonRegistro.setBounds(10, 448, 121, 50);
								panelLogin2.add(buttonRegistro);
								buttonLogin.setFont(new Font("Tahoma", Font.PLAIN, 16));
								// BOTÓN DEL LOGIN PARA CONFIRMAR EL USUARIO E IR A LA APP
								buttonLogin.addActionListener(new ActionListener() {
									public void actionPerformed(ActionEvent e) {
										loginEntrarEnApp();
									}
								});
								buttonLogin.setBounds(645, 448, 121, 50);
								panelLogin2.add(buttonLogin);
								btnSalir2.addActionListener(new ActionListener() {
									public void actionPerformed(ActionEvent e) {
										int resp = JOptionPane.showConfirmDialog(null, "Seguro que desea salir?");
										if (JOptionPane.OK_OPTION == resp) {
											System.exit(0);
										}
									}
								});
								btnSalir2.setIcon(new ImageIcon(ProyectoFinalJoseLuis.class.getResource("/Im\u00E1genes/salir.png")));
								btnSalir2.setBounds(10, 11, 76, 50);
								panelLogin2.add(btnSalir2);
						// REGISTRO
						panelRegistro3.setBounds(0, 0, 776, 509);
						frmBreakingnews.getContentPane().add(panelRegistro3);
						panelRegistro3.setLayout(null);
						labelRegistro.setForeground(Color.BLUE);
						labelRegistro.setFont(new Font("Snap ITC", Font.PLAIN, 35));
						labelRegistro.setBounds(218, 11, 320, 105);
						panelRegistro3.add(labelRegistro);
						labelUsuarioRegistro.setFont(new Font("Snap ITC", Font.PLAIN, 20));
						labelUsuarioRegistro.setBounds(181, 134, 94, 35);
						panelRegistro3.add(labelUsuarioRegistro);
						labelContraseñaRegistro.setFont(new Font("Snap ITC", Font.PLAIN, 20));
						labelContraseñaRegistro.setBounds(166, 228, 109, 30);
						panelRegistro3.add(labelContraseñaRegistro);
						labelEmailRegistro.setFont(new Font("Snap ITC", Font.PLAIN, 20));
						labelEmailRegistro.setBounds(192, 326, 83, 28);
						panelRegistro3.add(labelEmailRegistro);
						txtRegistroPassword = new JPasswordField();
						txtRegistroPassword.setBounds(285, 227, 235, 31);
						panelRegistro3.add(txtRegistroPassword);
						txtRegistroUsuario = new JTextField();
						txtRegistroUsuario.setBounds(285, 134, 235, 31);
						panelRegistro3.add(txtRegistroUsuario);
						txtRegistroUsuario.setColumns(10);
						txtRegistroEmail = new JTextField();
						txtRegistroEmail.setBounds(285, 326, 235, 30);
						panelRegistro3.add(txtRegistroEmail);
						txtRegistroEmail.setColumns(10);
						// BOTÓN VOLVER DESDE EL REGISTRO AL LOGIN
						buttonVolverLogin.setFont(new Font("Tahoma", Font.PLAIN, 16));
						buttonVolverLogin.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								panelLogin2.setVisible(true);
								panelRegistro3.setVisible(false);
								txtRegistroUsuario.setText("");
								txtRegistroPassword.setText("");
								txtRegistroEmail.setText("");
								txtUsuario.setText("");
								txtPassword.setText("");
							}
						});
						buttonVolverLogin.setBounds(10, 448, 121, 50);
						panelRegistro3.add(buttonVolverLogin);
						// botón Guardar de Registro //REGISTRO
						JButton btnNewButton = new JButton("Guardar");
						btnNewButton.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								guardarEnElRegistro();
							}
						});
						btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 16));
						btnNewButton.setBounds(645, 447, 121, 53);
						panelRegistro3.add(btnNewButton);
						btnSalir3.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								int resp = JOptionPane.showConfirmDialog(null, "Seguro que desea salir?");
								if (JOptionPane.OK_OPTION == resp) {
									System.exit(0);
								}
							}
						});
						btnSalir3.setIcon(new ImageIcon(ProyectoFinalJoseLuis.class.getResource("/Im\u00E1genes/salir.png")));
						btnSalir3.setBounds(10, 11, 76, 50);
						panelRegistro3.add(btnSalir3);
		panelTitulo1.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		// PANEL TITULO
		panelTitulo1.setBounds(0, 0, 776, 509);
		frmBreakingnews.getContentPane().add(panelTitulo1);
		panelTitulo1.setLayout(null);
		// TITULO BARRA DE PROGRESO
		JProgressBar progressBar = new JProgressBar(0, 100);
		progressBar.setForeground(new Color(0, 255, 0));
		progressBar.setBounds(162, 307, 482, 100);
		panelTitulo1.add(progressBar);
		// TÍTULO
		JLabel LabelTitulo = new JLabel("BREAKING NEWS", SwingConstants.CENTER);
		LabelTitulo.setForeground(Color.WHITE);
		LabelTitulo.setFont(new Font("Snap ITC", Font.PLAIN, 50));
		LabelTitulo.setBounds(110, 11, 561, 88);
		panelTitulo1.add(LabelTitulo);
		btonStart.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btonStart.setIcon(new ImageIcon(
				ProyectoFinalJoseLuis.class.getResource("/Im\u00E1genes/353434-off-on-power-switch_107506.png")));
		// BOTÓN START TÍTULO, PARA ACCIONAR LA BARRA DE PROGRESO
		btonStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				timer.start(); // Empieza a contar la barra de progreso
			}
		});
		btonStart.setBounds(316, 418, 162, 80);
		panelTitulo1.add(btonStart);
		btnSalir1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int resp = JOptionPane.showConfirmDialog(null, "Seguro que desea salir?");
				if (JOptionPane.OK_OPTION == resp) {
					System.exit(0);
				}
			}
		});
		btnSalir1.setIcon(new ImageIcon(ProyectoFinalJoseLuis.class.getResource("/Im\u00E1genes/salir.png")));
		btnSalir1.setBounds(10, 11, 76, 50);
		panelTitulo1.add(btnSalir1);
		lblImagen.setIcon(new ImageIcon(ProyectoFinalJoseLuis.class.getResource("/Im\u00E1genes/anime.jpg")));
		lblImagen.setBounds(0, 0, 776, 509);
		panelTitulo1.add(lblImagen);
		// ADMINISTRADOR USUARIOS
		panelAdministradorUsuarios6.setBounds(0, 0, 776, 508);
		frmBreakingnews.getContentPane().add(panelAdministradorUsuarios6);
		panelAdministradorUsuarios6.setLayout(null);
		JLabel lblNewLabel = new JLabel("Administrador Usuarios", SwingConstants.CENTER);
		lblNewLabel.setForeground(Color.BLUE);
		lblNewLabel.setFont(new Font("Snap ITC", Font.PLAIN, 35));
		lblNewLabel.setBounds(119, 11, 542, 60);
		panelAdministradorUsuarios6.add(lblNewLabel);
		buttonSalirDeAdmin.setFont(new Font("Tahoma", Font.PLAIN, 16));
		// BOTÓN SALIR DE ADMINISTRADOR, VUELVE AL INICIO DE SESIÓN
		buttonSalirDeAdmin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				volverInicioDesdeAdmin();
			}
		});
		buttonSalirDeAdmin.setBounds(10, 447, 121, 51);
		panelAdministradorUsuarios6.add(buttonSalirDeAdmin);
		buttonSiguienteNoticiasAdmin.setFont(new Font("Tahoma", Font.PLAIN, 16));
		// BOTÓN SIGUIENTE PARA IR A ADMINISTRADOR DE NOTICIAS
		buttonSiguienteNoticiasAdmin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				irAdminNoticias();
			}
		});
		buttonSiguienteNoticiasAdmin.setBounds(645, 447, 121, 51);
		panelAdministradorUsuarios6.add(buttonSiguienteNoticiasAdmin);
		btnSalir6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int resp = JOptionPane.showConfirmDialog(null, "Seguro que desea salir?");
				if (JOptionPane.OK_OPTION == resp) {
					System.exit(0);
				}
			}
		});
		btnSalir6.setIcon(new ImageIcon(ProyectoFinalJoseLuis.class.getResource("/Im\u00E1genes/salir.png")));
		btnSalir6.setBounds(10, 11, 76, 50);
		panelAdministradorUsuarios6.add(btnSalir6);
		ElegirUsuario1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				elegir1(); // Elegir Usuario1
			}
		});
		ElegirUsuario1.setVisible(false);

		ElegirUsuario1.setFont(new Font("Snap ITC", Font.PLAIN, 20));
		ElegirUsuario1.setBounds(10, 97, 159, 33);
		panelAdministradorUsuarios6.add(ElegirUsuario1);
		ElegirUsuario2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				elegir2(); // Elegir Usuario2
			}
		});
		ElegirUsuario2.setVisible(false);

		ElegirUsuario2.setFont(new Font("Snap ITC", Font.PLAIN, 20));
		ElegirUsuario2.setBounds(301, 97, 159, 33);
		panelAdministradorUsuarios6.add(ElegirUsuario2);
		ElegirUsuario3.setVisible(false);
		ElegirUsuario3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				elegir3(); // Elegir Usuario3
			}
		});
		ElegirUsuario3.setFont(new Font("Snap ITC", Font.PLAIN, 20));
		ElegirUsuario3.setBounds(607, 97, 159, 33);
		panelAdministradorUsuarios6.add(ElegirUsuario3);
		bxElMundoDep.setVisible(false);
		bxElMundoDep.setFont(new Font("Tahoma", Font.PLAIN, 16));
		bxElMundoDep.setBounds(6, 210, 183, 33);
		panelAdministradorUsuarios6.add(bxElMundoDep);
		bxElPaisDep.setVisible(false);
		bxElPaisDep.setFont(new Font("Tahoma", Font.PLAIN, 16));
		bxElPaisDep.setBounds(6, 283, 183, 33);
		panelAdministradorUsuarios6.add(bxElPaisDep);
		bxMarcaDep.setVisible(false);
		bxMarcaDep.setFont(new Font("Tahoma", Font.PLAIN, 16));
		bxMarcaDep.setBounds(10, 361, 183, 33);
		panelAdministradorUsuarios6.add(bxMarcaDep);
		bxElPaisArq.setVisible(false);
		bxElPaisArq.setFont(new Font("Tahoma", Font.PLAIN, 16));
		bxElPaisArq.setBounds(315, 210, 183, 33);
		panelAdministradorUsuarios6.add(bxElPaisArq);
		bxABCArq.setVisible(false);
		bxABCArq.setFont(new Font("Tahoma", Font.PLAIN, 16));
		bxABCArq.setBounds(315, 281, 183, 37);
		panelAdministradorUsuarios6.add(bxABCArq);
		bx20MinArq.setVisible(false);
		bx20MinArq.setFont(new Font("Tahoma", Font.PLAIN, 16));
		bx20MinArq.setBounds(315, 359, 183, 37);
		panelAdministradorUsuarios6.add(bx20MinArq);
		bxMisionTokyoAni.setVisible(false);
		bxMisionTokyoAni.setFont(new Font("Tahoma", Font.PLAIN, 16));
		bxMisionTokyoAni.setBounds(583, 210, 183, 33);
		panelAdministradorUsuarios6.add(bxMisionTokyoAni);
		bxRepublicaAni.setVisible(false);
		bxRepublicaAni.setFont(new Font("Tahoma", Font.PLAIN, 16));
		bxRepublicaAni.setBounds(583, 283, 183, 33);
		panelAdministradorUsuarios6.add(bxRepublicaAni);
		bxRamenPara2.setVisible(false);
		bxRamenPara2.setFont(new Font("Tahoma", Font.PLAIN, 16));
		bxRamenPara2.setBounds(583, 361, 183, 33);
		panelAdministradorUsuarios6.add(bxRamenPara2);
		lblAdminDep.setIcon(new ImageIcon(ProyectoFinalJoseLuis.class.getResource("/Im\u00E1genes/deporte.png")));
		lblAdminDep.setFont(new Font("Snap ITC", Font.PLAIN, 20));
		lblAdminDep.setBounds(10, 147, 182, 35);
		panelAdministradorUsuarios6.add(lblAdminDep);
		lblAdminArqu.setIcon(new ImageIcon(ProyectoFinalJoseLuis.class.getResource("/Im\u00E1genes/hueso.png")));
		lblAdminArqu.setFont(new Font("Snap ITC", Font.PLAIN, 20));
		lblAdminArqu.setBounds(289, 147, 182, 35);
		panelAdministradorUsuarios6.add(lblAdminArqu);
		lblAdminAni.setIcon(new ImageIcon(ProyectoFinalJoseLuis.class.getResource("/Im\u00E1genes/porpcorn.png")));
		lblAdminAni.setFont(new Font("Snap ITC", Font.PLAIN, 20));
		lblAdminAni.setBounds(584, 147, 182, 35);
		panelAdministradorUsuarios6.add(lblAdminAni);
		btnGuardarAdminUsu.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnGuardarAdminUsu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				guardarNoticiasDesdeAdmin();
			}
		});
		btnGuardarAdminUsu.setBounds(315, 449, 121, 51);
		panelAdministradorUsuarios6.add(btnGuardarAdminUsu);
		// ELEGIR NOTICIAS
		panelMostrarPreferencias5.setBounds(0, 0, 776, 509);
		frmBreakingnews.getContentPane().add(panelMostrarPreferencias5);
		panelMostrarPreferencias5.setLayout(null);
		// ELEGIR NOTICIAS
		JLabel labelTituloElegirNoticias = new JLabel("Tus preferencias son:", SwingConstants.CENTER);
		labelTituloElegirNoticias.setForeground(Color.BLUE);
		labelTituloElegirNoticias.setFont(new Font("Snap ITC", Font.PLAIN, 30));
		labelTituloElegirNoticias.setBounds(195, 11, 370, 105);
		panelMostrarPreferencias5.add(labelTituloElegirNoticias);

		// BOTÓN SALIR DESDE "ELEGIR NOTICIAS"
		buttonSalirElegirNoticias.setFont(new Font("Tahoma", Font.PLAIN, 16));
		buttonSalirElegirNoticias.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				salirDesdeElegirNoticias();
			}
		});
		buttonSalirElegirNoticias.setBounds(10, 448, 121, 50);
		panelMostrarPreferencias5.add(buttonSalirElegirNoticias);

		// label mostrar preferencias deporte
		lblMostrarPreferenciasDeporte
				.setIcon(new ImageIcon(ProyectoFinalJoseLuis.class.getResource("/Im\u00E1genes/deporte.png")));
		lblMostrarPreferenciasDeporte.setFont(new Font("Snap ITC", Font.PLAIN, 20));
		lblMostrarPreferenciasDeporte.setBounds(10, 156, 134, 35);
		panelMostrarPreferencias5.add(lblMostrarPreferenciasDeporte);

		// label mostrar preferencias arqueologia
		lblMostrarPreferenciaArqueologia
				.setIcon(new ImageIcon(ProyectoFinalJoseLuis.class.getResource("/Im\u00E1genes/hueso.png")));
		lblMostrarPreferenciaArqueologia.setFont(new Font("Snap ITC", Font.PLAIN, 20));
		lblMostrarPreferenciaArqueologia.setBounds(10, 242, 182, 35);
		panelMostrarPreferencias5.add(lblMostrarPreferenciaArqueologia);

		// label mostrar preferencias anime
		lblMostrarPreferenciaAnime
				.setIcon(new ImageIcon(ProyectoFinalJoseLuis.class.getResource("/Im\u00E1genes/porpcorn.png")));
		lblMostrarPreferenciaAnime.setFont(new Font("Snap ITC", Font.PLAIN, 20));
		lblMostrarPreferenciaAnime.setBounds(10, 339, 134, 35);
		panelMostrarPreferencias5.add(lblMostrarPreferenciaAnime);
		lblDeportePeriodico1.setVisible(false);

		// label deporte
		lblDeportePeriodico1.setFont(new Font("Snap ITC", Font.PLAIN, 16));
		lblDeportePeriodico1.setBounds(225, 158, 155, 33);
		panelMostrarPreferencias5.add(lblDeportePeriodico1);
		lblDeportePeriodico3.setVisible(false);

		// label deporte
		lblDeportePeriodico3.setFont(new Font("Snap ITC", Font.PLAIN, 16));
		lblDeportePeriodico3.setBounds(622, 158, 154, 35);
		panelMostrarPreferencias5.add(lblDeportePeriodico3);
		lblDeportePeriodico2.setVisible(false);

		// label deporte
		lblDeportePeriodico2.setFont(new Font("Snap ITC", Font.PLAIN, 16));
		lblDeportePeriodico2.setBounds(431, 158, 155, 35);
		panelMostrarPreferencias5.add(lblDeportePeriodico2);
		lblArqueologíaPeriodico1.setVisible(false);

		// Label arqueología
		lblArqueologíaPeriodico1.setFont(new Font("Snap ITC", Font.PLAIN, 16));
		lblArqueologíaPeriodico1.setBounds(225, 244, 155, 35);
		panelMostrarPreferencias5.add(lblArqueologíaPeriodico1);
		lblArqueologíaPeriodico2.setVisible(false);

		// Label arqueología
		lblArqueologíaPeriodico2.setFont(new Font("Snap ITC", Font.PLAIN, 16));
		lblArqueologíaPeriodico2.setBounds(431, 244, 155, 35);
		panelMostrarPreferencias5.add(lblArqueologíaPeriodico2);
		lblArqueologíaPeriodico3.setVisible(false);

		// Label arqueología
		lblArqueologíaPeriodico3.setFont(new Font("Snap ITC", Font.PLAIN, 16));
		lblArqueologíaPeriodico3.setBounds(622, 244, 154, 35);
		panelMostrarPreferencias5.add(lblArqueologíaPeriodico3);
		lblAnimePeriódico3.setVisible(false);

		// Label anime ramen para 2
		lblAnimePeriódico3.setFont(new Font("Snap ITC", Font.PLAIN, 16));
		lblAnimePeriódico3.setBounds(622, 341, 154, 35);
		panelMostrarPreferencias5.add(lblAnimePeriódico3);
		lblAnimePeriódico2.setVisible(false);

		// Label anime republica
		lblAnimePeriódico2.setFont(new Font("Snap ITC", Font.PLAIN, 16));
		lblAnimePeriódico2.setBounds(431, 341, 155, 35);
		panelMostrarPreferencias5.add(lblAnimePeriódico2);
		lblAnimePeriódico1.setVisible(false);

		// Label AnimeMisionTokyo
		lblAnimePeriódico1.setFont(new Font("Snap ITC", Font.PLAIN, 16));
		lblAnimePeriódico1.setBounds(225, 341, 155, 35);
		panelMostrarPreferencias5.add(lblAnimePeriódico1);
		btnSalir5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int resp = JOptionPane.showConfirmDialog(null, "Seguro que desea salir?");
				if (JOptionPane.OK_OPTION == resp) {
					System.exit(0);
				}
			}
		});
		btnSalir5.setIcon(new ImageIcon(ProyectoFinalJoseLuis.class.getResource("/Im\u00E1genes/salir.png")));
		btnSalir5.setBounds(10, 11, 76, 50);

		panelMostrarPreferencias5.add(btnSalir5);
		panelElegirNoticias4.setVisible(false);
		panelElegirNoticias4.setForeground(Color.BLACK);
		// MOSTRAR NOTICIAS
		panelElegirNoticias4.setBounds(0, 0, 776, 509);
		frmBreakingnews.getContentPane().add(panelElegirNoticias4);
		panelElegirNoticias4.setLayout(null);
		// MOSTRAR NOTICIAS TÍTULO MOSTRAR NOTICIAS
		JLabel labelTituloNoticias = new JLabel("Elegir peri\u00F3dicos", SwingConstants.CENTER);
		labelTituloNoticias.setForeground(Color.BLUE);
		labelTituloNoticias.setFont(new Font("Snap ITC", Font.PLAIN, 35));
		labelTituloNoticias.setBounds(184, 11, 391, 105);
		panelElegirNoticias4.add(labelTituloNoticias);
		buttonCerrarSesión.setFont(new Font("Tahoma", Font.PLAIN, 16));
		// BOTÓN SALIR DE MOSTRAR NOTICIAS, PARA VOLVER AL LOGIN
		buttonCerrarSesión.addActionListener(new ActionListener() {
			// Para que no se vea nada si inicio sesión con otro usuario, o me salgo sin
			// guardar
			public void actionPerformed(ActionEvent e) {
				salirDeMostrarNoticias();
			}
		});
		buttonCerrarSesión.setBounds(10, 448, 121, 50);
		panelElegirNoticias4.add(buttonCerrarSesión);
		JButton btnNewButton_1 = new JButton("Guardar Noticias");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				guardarNoticiasUsuario(); // Guardar las noticias del usuario
			}
		});
		btnNewButton_1.setBounds(645, 448, 121, 50);
		panelElegirNoticias4.add(btnNewButton_1);
		JLabel lblElegirNoticiasDeporte = new JLabel("Deporte");
		lblElegirNoticiasDeporte
				.setIcon(new ImageIcon(ProyectoFinalJoseLuis.class.getResource("/Im\u00E1genes/deporte.png")));
		lblElegirNoticiasDeporte.setFont(new Font("Snap ITC", Font.PLAIN, 20));
		lblElegirNoticiasDeporte.setBounds(10, 156, 182, 35);
		panelElegirNoticias4.add(lblElegirNoticiasDeporte);
		JLabel lblElegirNoticiasArqueología = new JLabel("Arqueolog\u00EDa");
		lblElegirNoticiasArqueología
				.setIcon(new ImageIcon(ProyectoFinalJoseLuis.class.getResource("/Im\u00E1genes/hueso.png")));
		lblElegirNoticiasArqueología.setFont(new Font("Snap ITC", Font.PLAIN, 20));
		lblElegirNoticiasArqueología.setBounds(278, 156, 182, 35);
		panelElegirNoticias4.add(lblElegirNoticiasArqueología);
		JLabel lblElegirNoticiasAnime = new JLabel("Anime");
		lblElegirNoticiasAnime
				.setIcon(new ImageIcon(ProyectoFinalJoseLuis.class.getResource("/Im\u00E1genes/porpcorn.png")));
		lblElegirNoticiasAnime.setFont(new Font("Snap ITC", Font.PLAIN, 20));
		lblElegirNoticiasAnime.setBounds(584, 156, 182, 35);
		panelElegirNoticias4.add(lblElegirNoticiasAnime);
		chckbxDeporteElMundo.setFont(new Font("Tahoma", Font.PLAIN, 16));
		chckbxDeporteElMundo.setBounds(10, 224, 182, 35);
		panelElegirNoticias4.add(chckbxDeporteElMundo);
		chckbxDeporteElPaís.setFont(new Font("Tahoma", Font.PLAIN, 16));
		chckbxDeporteElPaís.setBounds(10, 302, 182, 35);
		panelElegirNoticias4.add(chckbxDeporteElPaís);
		chckbxDeporteMarca.setFont(new Font("Tahoma", Font.PLAIN, 16));
		chckbxDeporteMarca.setBounds(10, 385, 182, 35);
		panelElegirNoticias4.add(chckbxDeporteMarca);
		chckbxArqueologíaElPaís.setFont(new Font("Tahoma", Font.PLAIN, 16));
		chckbxArqueologíaElPaís.setBounds(278, 224, 182, 35);
		panelElegirNoticias4.add(chckbxArqueologíaElPaís);
		chckbxArqueologíaABC.setFont(new Font("Tahoma", Font.PLAIN, 16));
		chckbxArqueologíaABC.setBounds(278, 302, 182, 35);
		panelElegirNoticias4.add(chckbxArqueologíaABC);
		chckbxArqueología20Minutos.setFont(new Font("Tahoma", Font.PLAIN, 16));
		chckbxArqueología20Minutos.setBounds(278, 385, 182, 35);
		panelElegirNoticias4.add(chckbxArqueología20Minutos);
		chckbxAnimeMisionTokyo.setFont(new Font("Tahoma", Font.PLAIN, 16));
		chckbxAnimeMisionTokyo.setBounds(584, 224, 182, 35);
		panelElegirNoticias4.add(chckbxAnimeMisionTokyo);
		chckbxAnimeRepublica.setFont(new Font("Tahoma", Font.PLAIN, 16));
		chckbxAnimeRepublica.setBounds(584, 302, 182, 35);
		panelElegirNoticias4.add(chckbxAnimeRepublica);
		chckbxAnimeRamenPara2.setFont(new Font("Tahoma", Font.PLAIN, 16));
		chckbxAnimeRamenPara2.setBounds(584, 385, 182, 35);
		panelElegirNoticias4.add(chckbxAnimeRamenPara2);
		btnSalir4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int resp = JOptionPane.showConfirmDialog(null, "Seguro que desea salir?");
				if (JOptionPane.OK_OPTION == resp) {
					System.exit(0);
				}
			}
		});
		btnSalir4.setIcon(new ImageIcon(ProyectoFinalJoseLuis.class.getResource("/Im\u00E1genes/salir.png")));
		btnSalir4.setBounds(10, 11, 76, 50);
		panelElegirNoticias4.add(btnSalir4);
		timer = new Timer(150, new ActionListener() { //  150 Para que tarde 15 segundos
			public void actionPerformed(ActionEvent e) {
				i++;
				if (i == 90 && (!f1.exists() || !f2.exists() || !f3.exists())) {
					JOptionPane.showMessageDialog(null, "Los ficheros no existen", "Error en los ficheros", 0);
					System.exit(0);
				}
				progressBar.setValue(i); // Para que la cuenta la lleve la variable i, y se cargue en 15 segundos
				progressBar.repaint(); // Para que se vaya pintando
				if (i >= 100) {
					timer.stop(); // Cuando la barra se llene cambiará de panel
					panelTitulo1.setVisible(false);
					panelLogin2.setVisible(true);
				}
			}
		});
	}

	public void elegir2() {
		if (ElegirUsuario2.isSelected()) {
			bxMarcaDep.setSelected(false);
			bxElPaisDep.setSelected(false);
			bxElMundoDep.setSelected(false);
			bxElPaisArq.setSelected(false);
			bxABCArq.setSelected(false);
			bx20MinArq.setSelected(false);
			bxMisionTokyoAni.setSelected(false);
			bxRamenPara2.setSelected(false);
			bxRepublicaAni.setSelected(false);

			ElegirUsuario1.setSelected(false);
			ElegirUsuario3.setSelected(false);

			bxMarcaDep.setVisible(true);
			bxElPaisDep.setVisible(true);
			bxElMundoDep.setVisible(true);
			bxElPaisArq.setVisible(true);
			bxABCArq.setVisible(true);
			bx20MinArq.setVisible(true);
			bxMisionTokyoAni.setVisible(true);
			bxRamenPara2.setVisible(true);
			bxRepublicaAni.setVisible(true);
		}
	}

	public void elegir1() {
		if (ElegirUsuario1.isSelected()) {
			bxMarcaDep.setSelected(false);
			bxElPaisDep.setSelected(false);
			bxElMundoDep.setSelected(false);
			bxElPaisArq.setSelected(false);
			bxABCArq.setSelected(false);
			bx20MinArq.setSelected(false);
			bxMisionTokyoAni.setSelected(false);
			bxRamenPara2.setSelected(false);
			bxRepublicaAni.setSelected(false);

			ElegirUsuario2.setSelected(false);
			ElegirUsuario3.setSelected(false);

			bxMarcaDep.setVisible(true);
			bxElPaisDep.setVisible(true);
			bxElMundoDep.setVisible(true);
			bxElPaisArq.setVisible(true);
			bxABCArq.setVisible(true);
			bx20MinArq.setVisible(true);
			bxMisionTokyoAni.setVisible(true);
			bxRamenPara2.setVisible(true);
			bxRepublicaAni.setVisible(true);
		}
	}

	public void elegir3() {
		if (ElegirUsuario3.isSelected()) {
			bxMarcaDep.setSelected(false);
			bxElPaisDep.setSelected(false);
			bxElMundoDep.setSelected(false);
			bxElPaisArq.setSelected(false);
			bxABCArq.setSelected(false);
			bx20MinArq.setSelected(false);
			bxMisionTokyoAni.setSelected(false);
			bxRamenPara2.setSelected(false);
			bxRepublicaAni.setSelected(false);

			ElegirUsuario1.setSelected(false);
			ElegirUsuario2.setSelected(false);

			bxMarcaDep.setVisible(true);
			bxElPaisDep.setVisible(true);
			bxElMundoDep.setVisible(true);
			bxElPaisArq.setVisible(true);
			bxABCArq.setVisible(true);
			bx20MinArq.setVisible(true);
			bxMisionTokyoAni.setVisible(true);
			bxRamenPara2.setVisible(true);
			bxRepublicaAni.setVisible(true);
		}
	}

	public void volverInicioDesdeAdmin() {
		panelLogin2.setVisible(true);
		panelAdministradorUsuarios6.setVisible(false);
		txtUsuario.setText("");
		txtPassword.setText("");
		// para que no se vean
		bxMarcaDep.setVisible(false);
		bxElPaisDep.setVisible(false);
		bxElMundoDep.setVisible(false);
		bxElPaisArq.setVisible(false);
		bxABCArq.setVisible(false);
		bx20MinArq.setVisible(false);
		bxMisionTokyoAni.setVisible(false);
		bxRamenPara2.setVisible(false);
		bxRepublicaAni.setVisible(false);
		// Para que no estén seleccionadas
		bxMarcaDep.setSelected(false);
		bxElPaisDep.setSelected(false);
		bxElMundoDep.setSelected(false);
		bxElPaisArq.setSelected(false);
		bxABCArq.setSelected(false);
		bx20MinArq.setSelected(false);
		bxMisionTokyoAni.setSelected(false);
		bxRamenPara2.setSelected(false);
		bxRepublicaAni.setSelected(false);
		// para que ningun usuario este seleccionado
		ElegirUsuario1.setSelected(false);
		ElegirUsuario2.setSelected(false);
		ElegirUsuario3.setSelected(false);
	}

	public void irAdminNoticias() { // para que no se vean
		bxMarcaDep.setVisible(false);
		bxElPaisDep.setVisible(false);
		bxElMundoDep.setVisible(false);
		bxElPaisArq.setVisible(false);
		bxABCArq.setVisible(false);
		bx20MinArq.setVisible(false);
		bxMisionTokyoAni.setVisible(false);
		bxRamenPara2.setVisible(false);
		bxRepublicaAni.setVisible(false); // Para que no estén seleccionadas
		bxMarcaDep.setSelected(false);
		bxElPaisDep.setSelected(false);
		bxElMundoDep.setSelected(false);
		bxElPaisArq.setSelected(false);
		bxABCArq.setSelected(false);
		bx20MinArq.setSelected(false);
		bxMisionTokyoAni.setSelected(false);
		bxRamenPara2.setSelected(false);
		bxRepublicaAni.setSelected(false); // para que ningun usuario este seleccionado
		ElegirUsuario1.setSelected(false);
		ElegirUsuario2.setSelected(false);
		ElegirUsuario3.setSelected(false);
		panelAdministradorTesting7.setVisible(true);
		panelAdministradorUsuarios6.setVisible(false);
	}

	public void guardarNoticiasDesdeAdmin() {
		String NoticiasUsuario1 = "";
		String NoticiasUsuario2 = "";
		String NoticiasUsuario3 = "";
		boolean elegido = true; // Condicion de que tiene que haber al menos una
		if (!ElegirUsuario1.isSelected() && !ElegirUsuario2.isSelected() && !ElegirUsuario3.isSelected()) {
			JOptionPane.showMessageDialog(null, "Elige a un usuario ", "Se tiene que elegir al menos a un usuario", 2);
		} else if (!bx20MinArq.isSelected() && !bxABCArq.isSelected() && !bxElPaisArq.isSelected()
				&& !bxElMundoDep.isSelected() && !bxElPaisDep.isSelected() && !bxMarcaDep.isSelected()
				&& !bxMisionTokyoAni.isSelected() && !bxRamenPara2.isSelected() && !bxRepublicaAni.isSelected()) {
			elegido = false;
			JOptionPane.showMessageDialog(null, "Se tiene que elegir al menos una noticia",
					"Elige las noticias de los usuarios :)", 2);
		} else {
			int resp = JOptionPane.showConfirmDialog(null, "¿Quieres guardar los cambios?");
			if (JOptionPane.OK_OPTION == resp && elegido) { // Creo las variables con las que voy a hacer los cambios
															// ,máximo 20 pero da igual, son 3 usuarios
				String contenidoLineasUsuarios[] = new String[20];
				int posicionLineaf2 = 0;
				int posicionLineaf1 = 0;
				String contenidoSplit1[] = new String[20];
				String contenidoSplit2[] = new String[20];
				String contenidoSplit3[] = new String[20];
				String contenidoLineasNoticias[] = new String[20];
				String contenidoSplitf11[] = new String[20];
				String contenidoSplitf12[] = new String[20];
				String contenidoSplitf13[] = new String[20]; // Leo los dos ficheros, para comparar
				try {
					FileReader fr2 = new FileReader(f2);
					BufferedReader br2 = new BufferedReader(fr2);
					String lineaf2 = "";
					String lineaf1 = "";
					FileReader fr1 = new FileReader(f1);
					BufferedReader br1 = new BufferedReader(fr1); // leo el fichero de usuarios
					lineaf2 = br2.readLine();
					while (lineaf2 != null) {
						contenidoLineasUsuarios[posicionLineaf2] = lineaf2;
						posicionLineaf2++;
						lineaf2 = br2.readLine();
					} // leo el fichero de noticias
					lineaf1 = br1.readLine();
					while (lineaf1 != null) {
						contenidoLineasNoticias[posicionLineaf1] = lineaf1;
						posicionLineaf1++;
						lineaf1 = br1.readLine();
					}
					br1.close();
					br2.close();
				} catch (NullPointerException e2) {
					JOptionPane.showMessageDialog(null, "ERROR EN FICHEROS","ERROR" , 0);
				} catch (IOException e2) {
					JOptionPane.showMessageDialog(null, "ERROR EN FICHEROS","ERROR" , 0);
				}
				if (contenidoLineasUsuarios[1] != null) { // hago el split para coger los campos de los 3 primeros
															// usuarios, quitando el admin
					contenidoSplit1 = contenidoLineasUsuarios[1].split(";");
				}
				if (contenidoLineasUsuarios[2] != null) {
					contenidoSplit2 = contenidoLineasUsuarios[2].split(";");
				}
				if (contenidoLineasUsuarios[3] != null) {
					contenidoSplit3 = contenidoLineasUsuarios[3].split(";");
				} // hago el split del fichero de noticias
				if (contenidoLineasNoticias[0] != null) {
					contenidoSplitf11 = contenidoLineasNoticias[0].split(";");
				}
				if (contenidoLineasNoticias[1] != null) {
					contenidoSplitf12 = contenidoLineasNoticias[1].split(";");
				}
				if (contenidoLineasNoticias[2] != null) {
					contenidoSplitf13 = contenidoLineasNoticias[2].split(";");
				}
				boolean usuario1 = false;
				boolean usuario2 = false;
				boolean usuario3 = false;
				boolean encontrado1 = false;
				boolean encontrado2 = false;
				boolean encontrado3 = false; // Si se elige el usuario 1
				if (ElegirUsuario1.isSelected()) {
					usuario1 = true; // comparo primero el nombre del fichero con el nombre del boton, y luego la
										// contraseña
					if (contenidoSplitf11[0] != null) {
						if (contenidoSplit1[0].equalsIgnoreCase(ElegirUsuario1.getText())
								&& contenidoSplitf11[0].equalsIgnoreCase(contenidoSplit1[0])
								&& contenidoSplitf11[1].equalsIgnoreCase(contenidoSplit1[1])) {
							NoticiasUsuario1 += contenidoSplit1[0] + ";" + contenidoSplit1[1] + ";" + contenidoSplit1[2]
									+ ";";
							encontrado1 = true; // lo comparo todo con el fichero de usuarios
						}
						if (contenidoSplit2[0] != null && !encontrado1) {
							if (contenidoSplit2[0].equalsIgnoreCase(ElegirUsuario1.getText())
									&& contenidoSplitf11[0].equalsIgnoreCase(contenidoSplit2[0])
									&& contenidoSplitf11[1].equalsIgnoreCase(contenidoSplit2[1])) {
								NoticiasUsuario1 += contenidoSplit2[0] + ";" + contenidoSplit2[1] + ";"
										+ contenidoSplit2[2] + ";";
								encontrado1 = true;
							}
						}
						if (contenidoSplit3[0] != null && !encontrado1) {
							if (contenidoSplit3[0].equalsIgnoreCase(ElegirUsuario1.getText())
									&& contenidoSplitf11[0].equalsIgnoreCase(contenidoSplit3[0])
									&& contenidoSplitf11[1].equalsIgnoreCase(contenidoSplit3[1])) {
								NoticiasUsuario1 += contenidoSplit3[0] + ";" + contenidoSplit3[1] + ";"
										+ contenidoSplit3[2] + ";";
								encontrado1 = true;
							}
						}
					}
					NoticiasUsuario1 += "DEPORTE;";
					if (bxElMundoDep.isSelected()) {
						NoticiasUsuario1 += "el mundo;";
					}
					if (bxElPaisDep.isSelected()) {
						NoticiasUsuario1 += "el PaísDepor;";
					}
					if (bxMarcaDep.isSelected()) {
						NoticiasUsuario1 += "marca;";
					}
					NoticiasUsuario1 += "ARQUEOLOGÍA;";
					if (bxElPaisArq.isSelected()) {
						NoticiasUsuario1 += "el país;";
					}
					if (bxABCArq.isSelected()) {
						NoticiasUsuario1 += "abc;";
					}
					if (bx20MinArq.isSelected()) {
						NoticiasUsuario1 += "20 minutos;";
					}
					NoticiasUsuario1 += "ANIME;";
					if (bxMisionTokyoAni.isSelected()) {
						NoticiasUsuario1 += "misiontokyo;";
					}
					if (bxRepublicaAni.isSelected()) {
						NoticiasUsuario1 += "republica;";
					}
					if (bxRamenPara2.isSelected()) {
						NoticiasUsuario1 += "ramen para 2;";
					}
				} // Si se elige el usuario 2
				if (ElegirUsuario2.isSelected()) {
					usuario2 = true;
					if (contenidoSplitf12[0] != null) {
						if (contenidoSplit1[0].equalsIgnoreCase(ElegirUsuario2.getText())
								&& contenidoSplitf12[0].equalsIgnoreCase(contenidoSplit1[0])
								&& contenidoSplitf12[1].equalsIgnoreCase(contenidoSplit1[1])) {
							NoticiasUsuario2 += contenidoSplit1[0] + ";" + contenidoSplit1[1] + ";" + contenidoSplit1[2]
									+ ";";
							encontrado2 = true;
						}
						if (contenidoSplit2[0] != null && !encontrado2) {
							if (contenidoSplit2[0].equalsIgnoreCase(ElegirUsuario2.getText())
									&& contenidoSplitf12[0].equalsIgnoreCase(contenidoSplit2[0])
									&& contenidoSplitf12[1].equalsIgnoreCase(contenidoSplit2[1])) {
								NoticiasUsuario2 += contenidoSplit2[0] + ";" + contenidoSplit2[1] + ";"
										+ contenidoSplit2[2] + ";";
								encontrado2 = true;
							}
						}
						if (contenidoSplit3[0] != null && !encontrado2) {
							if (contenidoSplit3[0].equalsIgnoreCase(ElegirUsuario2.getText())
									&& contenidoSplitf12[0].equalsIgnoreCase(contenidoSplit3[0])
									&& contenidoSplitf12[1].equalsIgnoreCase(contenidoSplit3[1])) {
								NoticiasUsuario2 += contenidoSplit3[0] + ";" + contenidoSplit3[1] + ";"
										+ contenidoSplit3[2] + ";";
								encontrado2 = true;
							}
						}
					}
					NoticiasUsuario2 += "DEPORTE;";
					if (bxElMundoDep.isSelected()) {
						NoticiasUsuario2 += "el mundo;";
					}
					if (bxElPaisDep.isSelected()) {
						NoticiasUsuario2 += "el PaísDepor;";
					}
					if (bxMarcaDep.isSelected()) {
						NoticiasUsuario2 += "marca;";
					}
					NoticiasUsuario2 += "ARQUEOLOGÍA;";
					if (bxElPaisArq.isSelected()) {
						NoticiasUsuario2 += "el país;";
					}
					if (bxABCArq.isSelected()) {
						NoticiasUsuario2 += "abc;";
					}
					if (bx20MinArq.isSelected()) {
						NoticiasUsuario2 += "20 minutos;";
					}
					NoticiasUsuario2 += "ANIME;";
					if (bxMisionTokyoAni.isSelected()) {
						NoticiasUsuario2 += "misiontokyo;";
					}
					if (bxRepublicaAni.isSelected()) {
						NoticiasUsuario2 += "republica;";
					}
					if (bxRamenPara2.isSelected()) {
						NoticiasUsuario2 += "ramen para 2;";
					}
				} // Si se elige el usuario 3
				if (ElegirUsuario3.isSelected()) {
					usuario3 = true;
					if (contenidoSplitf13[0] != null) {
						if (contenidoSplit1[0].equalsIgnoreCase(ElegirUsuario3.getText())
								&& contenidoSplitf13[0].equalsIgnoreCase(contenidoSplit1[0])
								&& contenidoSplitf13[1].equalsIgnoreCase(contenidoSplit1[1])) {
							NoticiasUsuario3 += contenidoSplit1[0] + ";" + contenidoSplit1[1] + ";" + contenidoSplit1[2]
									+ ";";
							encontrado3 = true;
						}
						if (contenidoSplit2[0] != null && !encontrado3) {
							if (contenidoSplit2[0].equalsIgnoreCase(ElegirUsuario3.getText())
									&& contenidoSplitf13[0].equalsIgnoreCase(contenidoSplit2[0])
									&& contenidoSplitf13[1].equalsIgnoreCase(contenidoSplit2[1])) {
								NoticiasUsuario3 += contenidoSplit2[0] + ";" + contenidoSplit2[1] + ";"
										+ contenidoSplit2[2] + ";";
								encontrado3 = true;
							}
						}
						if (contenidoSplit3[0] != null && !encontrado3) {
							if (contenidoSplit3[0].equalsIgnoreCase(ElegirUsuario3.getText())
									&& contenidoSplitf13[0].equalsIgnoreCase(contenidoSplit3[0])
									&& contenidoSplitf13[1].equalsIgnoreCase(contenidoSplit3[1])) {
								NoticiasUsuario3 += contenidoSplit3[0] + ";" + contenidoSplit3[1] + ";"
										+ contenidoSplit3[2] + ";";
								encontrado3 = true;
							}
						}
					}
					NoticiasUsuario3 += "DEPORTE;";
					if (bxElMundoDep.isSelected()) {
						NoticiasUsuario3 += "el mundo;";
					}
					if (bxElPaisDep.isSelected()) {
						NoticiasUsuario3 += "el PaísDepor;";
					}
					if (bxMarcaDep.isSelected()) {
						NoticiasUsuario3 += "marca;";
					}
					NoticiasUsuario3 += "ARQUEOLOGÍA;";
					if (bxElPaisArq.isSelected()) {
						NoticiasUsuario3 += "el país;";
					}
					if (bxABCArq.isSelected()) {
						NoticiasUsuario3 += "abc;";
					}
					if (bx20MinArq.isSelected()) {
						NoticiasUsuario3 += "20 minutos;";
					}
					NoticiasUsuario3 += "ANIME;";
					if (bxMisionTokyoAni.isSelected()) {
						NoticiasUsuario3 += "misiontokyo;";
					}
					if (bxRepublicaAni.isSelected()) {
						NoticiasUsuario3 += "republica;";
					}
					if (bxRamenPara2.isSelected()) {
						NoticiasUsuario3 += "ramen para 2;";
					}
				}
				try { // Escribir en el fichero las noticias ya cambiadas
					FileWriter fw = new FileWriter(f1);
					BufferedWriter bw = new BufferedWriter(fw);
					if (usuario1 == true) {
						bw.write(NoticiasUsuario1);
						bw.newLine();
						if (contenidoLineasNoticias[1] != null) {
							bw.write(contenidoLineasNoticias[1]);
							bw.newLine();
						}
						if (contenidoLineasNoticias[2] != null) {
							bw.write(contenidoLineasNoticias[2]);
							bw.newLine();
						}
					}
					if (usuario2 == true) {
						if (contenidoLineasNoticias[0] != null) {
							bw.write(contenidoLineasNoticias[0]);
							bw.newLine();
						}
						bw.write(NoticiasUsuario2);
						bw.newLine();
						if (contenidoLineasNoticias[2] != null) {
							bw.write(contenidoLineasNoticias[2]);
							bw.newLine();
						}
					}
					if (usuario3 == true) {
						if (contenidoLineasNoticias[0] != null) {
							bw.write(contenidoLineasNoticias[0]);
							bw.newLine();
						}
						if (contenidoLineasNoticias[1] != null) {
							bw.write(contenidoLineasNoticias[1]);
							bw.newLine();
						}
						bw.write(NoticiasUsuario3);
						bw.newLine();
					}
					bw.close();
				} catch (NullPointerException e1) {
					JOptionPane.showMessageDialog(null, "ERROR EN FICHEROS","ERROR" , 0);
				} catch (IOException e1) {
					JOptionPane.showMessageDialog(null, "ERROR EN FICHEROS","ERROR" , 0);
				}
				JOptionPane.showMessageDialog(null, "Datos guardados correctamente", "Todo correcto", 1);
			}
			usuario.setNombre("");
			usuario.setPssw("");
		}
	}

	public void salirDesdeElegirNoticias() {
		// para que no se vea nada escrito cuando salgo de mostrar noticias
		lblArqueologíaPeriodico1.setVisible(false);
		lblArqueologíaPeriodico2.setVisible(false);
		lblArqueologíaPeriodico3.setVisible(false);
		lblDeportePeriodico1.setVisible(false);
		lblDeportePeriodico2.setVisible(false);
		lblDeportePeriodico3.setVisible(false);
		lblAnimePeriódico1.setVisible(false);
		lblAnimePeriódico2.setVisible(false);
		lblAnimePeriódico3.setVisible(false);
		chckbxDeporteMarca.setSelected(false);
		chckbxDeporteElPaís.setSelected(false);
		chckbxDeporteElMundo.setSelected(false);
		chckbxArqueologíaElPaís.setSelected(false);
		chckbxArqueologíaABC.setSelected(false);
		chckbxArqueología20Minutos.setSelected(false);
		chckbxAnimeMisionTokyo.setSelected(false);
		chckbxAnimeRamenPara2.setSelected(false);
		chckbxAnimeRepublica.setSelected(false);
		panelLogin2.setVisible(true);
		panelMostrarPreferencias5.setVisible(false);
		txtUsuario.setText("");
		txtPassword.setText("");
	}

	@SuppressWarnings("deprecation")
	public void guardarEnElRegistro() { // Para que no esté vacío ningún campo
		if (txtRegistroUsuario.getText().isEmpty() || txtRegistroPassword.getText().isEmpty()
				|| txtRegistroEmail.getText().isEmpty()) {
			JOptionPane.showMessageDialog(null, "Rellene con sus datos :)", "Algún campo está vacío", 0); // para que no
																											// se puedan
																											// poner
																											// algunos
																											// carácteres
		} else if (txtRegistroUsuario.getText().length() > 35 || txtRegistroPassword.getText().length() > 35
				|| txtRegistroEmail.getText().length() > 35) {
			JOptionPane.showMessageDialog(null, "Demasiados carácteres", "35 máximo", 0);
		} else if (txtRegistroUsuario.getText().contains(";") || txtRegistroUsuario.getText().contains("/")
				|| txtRegistroUsuario.getText().contains(" ") || txtRegistroUsuario.getText().contains("*")
				|| txtRegistroPassword.getText().contains(";") || txtRegistroPassword.getText().contains("/")
				|| txtRegistroPassword.getText().contains(" ") || txtRegistroPassword.getText().contains("*")
				|| txtRegistroEmail.getText().contains(";") || txtRegistroEmail.getText().contains("/")
				|| txtRegistroEmail.getText().contains(" ") || txtRegistroEmail.getText().contains("*")) {
			JOptionPane.showMessageDialog(null, "/,*,   ,; ", "Carácteres no admitidos", 0); // Para que se tenga que
																								// poner @gmail.com
		} else if (!txtRegistroEmail.getText().contains("@gmail.com")) {
			JOptionPane.showMessageDialog(null, "El correo necesita @gmail.com", "Introduzca @gmail.com", 1);
		} else {
			String nombre = txtRegistroUsuario.getText();
			String pssw = txtRegistroPassword.getText();
			String email = txtRegistroEmail.getText(); // Cambiamos el valor de las variables de la clase usuario
			usuario.setNombre(nombre);
			usuario.setPssw(pssw);
			usuario.setEmail(email);
			String[] parteLinea = null;
			boolean existe = false;
			try { // Leo el fichero para ver si existe algún usuario ya creado y no se repita
				FileReader fr = new FileReader(f2);
				BufferedReader bf = new BufferedReader(fr);
				String linea;
				boolean repetido = false;
				linea = bf.readLine();
				while (linea != null) {
					parteLinea = linea.split(";"); // lee el fichero y si existe ya no entra en crear usuario
					for (int j = 0; j < parteLinea.length; j++) {
						if (parteLinea[j].equalsIgnoreCase(txtRegistroUsuario.getText())
								&& parteLinea[j].equalsIgnoreCase(txtRegistroPassword.getText()) && !repetido) {
							JOptionPane.showMessageDialog(null, "Intente con otro nombre o contraseña",
									"Usuario existente", 1);
							txtRegistroUsuario.setText("");
							txtRegistroPassword.setText("");
							existe = true;
							repetido = true;
						}
					}
					linea = bf.readLine();
				}
				if (!existe) { // Si el usuario NO existe entra en esta función y lo crea // Se va añadiendo al
								// fichero
					FileWriter fw2 = new FileWriter(f2, true);
					fw2.write("\n");
					fw2.write(usuario.getNombre() + ";");
					fw2.write(usuario.getPssw() + ";");
					fw2.write(usuario.getEmail() + ";" + "/" + ";");
					fw2.close();
					JOptionPane.showMessageDialog(null, "Registro correcto :)", "Bienvenido", 1);
				}
				bf.close();
			} catch (FileNotFoundException e1) {
				JOptionPane.showMessageDialog(null, "ERROR EN FICHEROS","ERROR" , 0);
			} catch (IOException e2) {
				JOptionPane.showMessageDialog(null, "ERROR EN FICHEROS","ERROR" , 0);
			}
			txtRegistroUsuario.setText(null);
			txtRegistroPassword.setText(null);
			txtRegistroEmail.setText(null);
		}
	}

	@SuppressWarnings("deprecation")
	public void loginEntrarEnApp() {

		if (txtUsuario.getText().isEmpty() || txtPassword.getText().isEmpty()) {
			JOptionPane.showMessageDialog(null, "Rellene con sus datos", "Campo vacío", 0);
		} else {
			String linea, linea2;
			String[] parteLineafichero2 = null; // para hacer el .split()
			String[] parteLineafichero1 = null;
			boolean registrado = false;
			boolean tieneNoticias = false;
			boolean esAdmin = false;

			try {
				FileReader fr = new FileReader(f2);
				BufferedReader br = new BufferedReader(fr);
				linea = br.readLine();
				while (linea != null) {
					parteLineafichero2 = linea.split(";"); // separa el fichero por donde yo le diga
					if (parteLineafichero2[0].equalsIgnoreCase(txtUsuario.getText())
							&& parteLineafichero2[1].equals(txtPassword.getText())) {
						if (parteLineafichero2[3].equals("*")) {
							JOptionPane.showMessageDialog(null, "Bienvenido admin :)", "Administrador", 1);
							esAdmin = true;
							registrado = true;

							// Leer fichero para saber si ha escogido noticias
							String contenido = " ";
							String contenidoLineas[] = new String[20];
							int posicionLinea = 0;
							String contenidoSplit1[] = new String[20];
							String contenidoSplit2[] = new String[20];
							String contenidoSplit3[] = new String[20];

							FileReader fr1 = new FileReader(f1);
							BufferedReader br1 = new BufferedReader(fr1);

							contenido = br1.readLine();
							while (contenido != null) {

								contenidoLineas[posicionLinea] = contenido;
								posicionLinea++;

								contenido = br1.readLine();
							}
							br1.close();

							if (contenidoLineas[0] != null) {
								contenidoSplit1 = contenidoLineas[0].split(";");
								ElegirUsuario1.setText(contenidoSplit1[0]);
								ElegirUsuario1.setVisible(true);
							}
							if (contenidoLineas[1] != null) {

								contenidoSplit2 = contenidoLineas[1].split(";");

								ElegirUsuario2.setText(contenidoSplit2[0]);

								ElegirUsuario2.setVisible(true);

							}
							if (contenidoLineas[2] != null) {

								contenidoSplit3 = contenidoLineas[2].split(";");
								ElegirUsuario3.setText(contenidoSplit3[0]);

								ElegirUsuario3.setVisible(true);
							}

							panelAdministradorUsuarios6.setVisible(true);
							panelLogin2.setVisible(false);
						} else {
							JOptionPane.showMessageDialog(null, "Bienvenido " + parteLineafichero2[0] + " :)",
									"Usuario", 1);
							registrado = true; // para que no entre en el otro if
							usuario.setNombre(txtUsuario.getText());
							usuario.setPssw(txtPassword.getText());
							usuario.setEmail(parteLineafichero2[2]);

							// Para leer que no exista ese nombre en el fichero

							FileReader fr2 = new FileReader(f1);
							BufferedReader br2 = new BufferedReader(fr2);
							linea2 = br2.readLine();
							while (linea2 != null) {
								parteLineafichero1 = linea2.split(";");
								// si existe ese nombre+contraseña va directamente a mostrar preferencias
								if (parteLineafichero1[0].equalsIgnoreCase(usuario.getNombre())
										&& parteLineafichero1[1].equals(usuario.getPssw())) {
									// Para mostrar los labels activos si estan en el fichero
									if (linea2.contains("marca")) {

										lblDeportePeriodico3.setVisible(true);
									}
									if (linea2.contains("el PaísDepor")) {

										lblDeportePeriodico2.setVisible(true);
									}
									if (linea2.contains("el mundo")) {

										lblDeportePeriodico1.setVisible(true);
									}

									if (linea2.contains("el país")) {

										lblArqueologíaPeriodico1.setVisible(true);
									}
									if (linea2.contains("abc")) {

										lblArqueologíaPeriodico2.setVisible(true);
									}
									if (linea2.contains("20 minutos")) {

										lblArqueologíaPeriodico3.setVisible(true);
									}

									if (linea2.contains("misiontokyo")) {

										lblAnimePeriódico1.setVisible(true);
									}
									if (linea2.contains("ramen para 2")) {

										lblAnimePeriódico3.setVisible(true);
									}
									if (linea2.contains("republica")) {

										lblAnimePeriódico2.setVisible(true);
									}

									panelLogin2.setVisible(false);
									panelMostrarPreferencias5.setVisible(true);
									tieneNoticias = true;
									// si no existe se va a elegir noticias
								}
								linea2 = br2.readLine();
							}

							br2.close();
						}
					}
					linea = br.readLine();
				}

				if (!registrado) {
					JOptionPane.showMessageDialog(null, "Usuario o contraseña incorrecto", "Verifíque su cuenta", 2);
				}

				if (!tieneNoticias && registrado && !esAdmin) {
					panelElegirNoticias4.setVisible(true);
					panelLogin2.setVisible(false);
				}

				br.close();
			} catch (FileNotFoundException e1) {

				JOptionPane.showMessageDialog(null, "ERROR EN FICHEROS","ERROR" , 0);
			} catch (IOException e1) {

				JOptionPane.showMessageDialog(null, "ERROR EN FICHEROS","ERROR" , 0);
			}

		}
	}

	public void salirDeMostrarNoticias() {
		lblDeportePeriodico3.setVisible(false);
		lblDeportePeriodico2.setVisible(false);
		lblDeportePeriodico1.setVisible(false);
		lblArqueologíaPeriodico1.setVisible(false);
		lblArqueologíaPeriodico2.setVisible(false);
		lblArqueologíaPeriodico3.setVisible(false);
		lblAnimePeriódico1.setVisible(false);
		lblAnimePeriódico1.setVisible(false);
		lblAnimePeriódico1.setVisible(false);
		chckbxDeporteMarca.setSelected(false);
		chckbxDeporteElPaís.setSelected(false);
		chckbxDeporteElMundo.setSelected(false);
		chckbxArqueologíaElPaís.setSelected(false);
		chckbxArqueologíaABC.setSelected(false);
		chckbxArqueología20Minutos.setSelected(false);
		chckbxAnimeMisionTokyo.setSelected(false);
		chckbxAnimeRamenPara2.setSelected(false);
		chckbxAnimeRepublica.setSelected(false);
		txtUsuario.setText("");
		txtPassword.setText("");
		panelLogin2.setVisible(true);
		panelElegirNoticias4.setVisible(false);
	}

	public void guardarNoticiasUsuario() {
		boolean elegido = true; // Condicion de que tiene que haber al menos una noticia seleccionada
		if (!chckbxDeporteMarca.isSelected() && !chckbxDeporteElPaís.isSelected() && !chckbxDeporteElMundo.isSelected()
				&& !chckbxArqueologíaElPaís.isSelected() && !chckbxArqueologíaABC.isSelected()
				&& !chckbxArqueología20Minutos.isSelected() && !chckbxAnimeMisionTokyo.isSelected()
				&& !chckbxAnimeRamenPara2.isSelected() && !chckbxAnimeRepublica.isSelected()) {
			elegido = false;
			JOptionPane.showMessageDialog(null, "Tiene que elegir mínimo una noticia, si no desea seguir, pulse Salir",
					"Elije tus noticias :)", 2);
		} else {
			int resp = JOptionPane.showConfirmDialog(null, "Seguro que desea guardar?");
			if (JOptionPane.OK_OPTION == resp && elegido) {
				try {
					FileWriter fw = new FileWriter(f1, true);
					BufferedWriter bw = new BufferedWriter(fw); // Para escribir el correo en el fichero de
																// configuración de email desde elAdmin // Botón de
																// guardar preferencias, es decir que ya tiene
																// preferencias
					bw.write(usuario.getNombre() + ";");
					bw.write(usuario.getPssw() + ";"); // Para escribir el email en el fichero de noticias y sus
														// preferencias
					bw.write(usuario.getEmail() + ";");
					bw.write("DEPORTE" + ";");
					if (chckbxDeporteMarca.isSelected()) {
						bw.write("marca" + ";");
						lblDeportePeriodico3.setVisible(true);
					}
					if (chckbxDeporteElPaís.isSelected()) {
						bw.write("el PaísDepor" + ";");
						lblDeportePeriodico2.setVisible(true);
					}
					if (chckbxDeporteElMundo.isSelected()) {
						bw.write("el mundo" + ";");
						lblDeportePeriodico1.setVisible(true);
					}
					bw.write(" ARQUEOLOGÍA" + ";");
					if (chckbxArqueologíaElPaís.isSelected()) {
						bw.write("el país" + ";");
						lblArqueologíaPeriodico1.setVisible(true);
					}
					if (chckbxArqueologíaABC.isSelected()) {
						bw.write("abc" + ";");
						lblArqueologíaPeriodico2.setVisible(true);
					}
					if (chckbxArqueología20Minutos.isSelected()) {
						bw.write("20 minutos" + ";");
						lblArqueologíaPeriodico3.setVisible(true);
					}
					bw.write("ANIME" + ";");
					if (chckbxAnimeMisionTokyo.isSelected()) {
						bw.write("misiontokyo" + ";");
						lblAnimePeriódico1.setVisible(true);
					}
					if (chckbxAnimeRamenPara2.isSelected()) {
						bw.write("ramen para 2" + ";");
						lblAnimePeriódico3.setVisible(true);
					}
					if (chckbxAnimeRepublica.isSelected()) {
						bw.write("republica" + ";");
						lblAnimePeriódico2.setVisible(true);
					}
					bw.newLine();
					bw.close();
	 		        JOptionPane.showMessageDialog(null, "Tus noticias se han guardado correctamente", "Correcto", 1);
					btnGuardarAdminUsu.setVisible(true);

				} catch (NullPointerException e1) {
					JOptionPane.showMessageDialog(null, "ERROR EN FICHEROS","ERROR" , 0);
				} catch (IOException e1) {
					JOptionPane.showMessageDialog(null, "ERROR EN FICHEROS","ERROR" , 0);
				}
				panelMostrarPreferencias5.setVisible(true);
				panelElegirNoticias4.setVisible(false);
			}
		}
	}

	public void CambiarHora() {
		String linea;
		String parteLinea[];
		String hora;
		String lineaFicheroNuevo = "";
		if (txtHora.getText().isEmpty() || txtMin.getText().isEmpty()) {
			JOptionPane.showMessageDialog(null, "Algún campo está vacío", "Rellene los campos", 3);
		} else if (txtHora.getText().length() != 2 || txtMin.getText().length() != 2) {
			JOptionPane.showMessageDialog(null, "Tiene que escribir 2 números en cada texto", "Escriba 2 números", 3);
		} else if((Integer.parseInt(txtHora.getText())<0||Integer.parseInt(txtHora.getText())>23)||(Integer.parseInt(txtMin.getText())<0||Integer.parseInt(txtMin.getText())>59)){
			txtHora.setText("");
			txtMin.setText("");
			JOptionPane.showMessageDialog(null, "Compruebe que la hora que ingrese sea válida", "Hora Incorrecta", 3);
		}else {
			hora = txtHora.getText() + ":" + txtMin.getText();
			try {
				FileReader fr = new FileReader(f3);
				BufferedReader br = new BufferedReader(fr); // Leo el fichero
				linea = br.readLine();
				parteLinea = linea.split(";");
				br.close();
				parteLinea[2] = hora; // Sobreescribro la parte linea
				lineaFicheroNuevo += parteLinea[0] + ";" + parteLinea[1] + ";" + parteLinea[2] + ";";
				FileWriter fw = new FileWriter(f3);
				BufferedWriter bw = new BufferedWriter(fw); // Escribo en el fichero
				bw.write(lineaFicheroNuevo);
				bw.close();
			} catch (FileNotFoundException e1) {
				JOptionPane.showMessageDialog(null, "Error en los ficheros", "Error", 0);
			} catch (IOException e1) {
				JOptionPane.showMessageDialog(null, "Error en los ficheros", "Error", 0);
			}
		}
	}
}
