public class Usuario {

	private String nombre;
	private String pssw;
	private String email;
	

	public Usuario(String nombre, String pssw, String email, boolean admin) {
		
		this.nombre = nombre;
		this.pssw = pssw;
		this.email = email;
	}
	
	

	public Usuario() {
		
		nombre= " ";
		pssw= " ";
		email=" ";
	}



	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPssw() {
		return pssw;
	}

	public void setPssw(String pssw) {
		this.pssw = pssw;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	

}
