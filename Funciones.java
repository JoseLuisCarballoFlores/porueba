import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.ConnectException;
import java.net.Socket;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.swing.JOptionPane;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Funciones {	// NOTICIA DEPORTE1 EL MUNDO
	
public static String deporte1() {
		String cadena = null;
		Document document = null;
		String webPage = "https://www.elmundo.es/deportes.html";	
		try {			
			document = Jsoup.connect(webPage).get();
		}catch(ConnectException e) {
			
			JOptionPane.showMessageDialog(null,"No tiene internet","Asegúrese de que tiene conexión", 0);			
		} catch (IOException e) {

			JOptionPane.showMessageDialog(null, "ERROR AL BUSCAR LAS NOTICIAS","ERROR" , 0);
		}
		Elements palabra2 = document.getElementsByClass("ue-c-cover-content__headline").get(4).select("h2");
		cadena = palabra2.html();
		return cadena;
	}	                                         // NOTICIA DEPORTE2 EL PAÍS
	public static String deporte2() {
		String cadena = null;
		Document document = null;
		String webPage = "https://elpais.com/noticias/deportes-combate/";
		try {
			document = Jsoup.connect(webPage).get();
		}catch(ConnectException e) {
			
			JOptionPane.showMessageDialog(null,"No tiene internet","Asegúrese de que tiene conexión", 0);			
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "ERROR AL BUSCAR LAS NOTICIAS","ERROR" , 0);
		}
		Elements palabra2 = document.getElementsByClass("c_t").get(1).select("a");
		cadena = palabra2.html();
		return cadena;
	}	                                       // NOTICIA DEPORTE3 MARCA
	public static String deporte3() {
		String cadena = null;
		Document document = null;
		String webPage = "https://www.marca.com/mma.html";
		try {
			document = Jsoup.connect(webPage).get();
		}catch(ConnectException e) {
			
			JOptionPane.showMessageDialog(null,"No tiene internet","Asegúrese de que tiene conexión", 0);			
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "ERROR AL BUSCAR LAS NOTICIAS","ERROR" , 0);
		}
		Elements palabra2 = document.getElementsByClass("mod-title").get(2).select("a");
		cadena = palabra2.html();
		return cadena;
	}                                           // NOTICIA ARQUEOLOGÍA1 EL PAIS
	public static String arqueologia1() {
		String cadena = null;
		Document document = null;
		String webPage = "https://elpais.com/noticias/arqueologia/";
		try {
			document = Jsoup.connect(webPage).get();
		}catch(ConnectException e) {
		
			JOptionPane.showMessageDialog(null,"No tiene internet","Asegúrese de que tiene conexión", 0);			
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "ERROR AL BUSCAR LAS NOTICIAS","ERROR" , 0);
		}
		Elements palabra2 = document.getElementsByClass("c_t").get(0).select("a");
		cadena = palabra2.html();
		return cadena;
	}                                 	// NOTICIA ARQUEOLOGÍA2 ABC
	public static String arqueologia2() {
		String cadena = null;
		Document document = null;
		String webPage = "https://www.abc.es/cultura/arte/arqueologia/";
		try {
			document = Jsoup.connect(webPage).get();
		}catch(ConnectException e) {
			
			JOptionPane.showMessageDialog(null,"No tiene internet","Asegúrese de que tiene conexión", 0);			
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "ERROR AL BUSCAR LAS NOTICIAS","ERROR" , 0);
		}
		Elements palabra2 = document.getElementsByClass("titular size30").get(1).select("h1");
		cadena = palabra2.html();
		return cadena;
	}                                    	// NOTICIA ARQUEOLOGÍA3 20MINUTOS
	public static String arqueologia3() {
		String cadena = null;
		Document document = null;
		String webPage = "https://www.20minutos.es/minuteca/arqueologia/";
		try {
			document = Jsoup.connect(webPage).get();
		}catch(ConnectException e) {
			
			JOptionPane.showMessageDialog(null,"No tiene internet","Asegúrese de que tiene conexión", 0);			
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "ERROR AL BUSCAR LAS NOTICIAS","ERROR" , 0);
		}
		Elements palabra2 = document.getElementsByClass("media-content").get(2).select("a");
		cadena = palabra2.html();
		return cadena;
	}                                      // NOTICIA ANIME1 misiontokyo
	public static String anime1() {
		String cadena = null;
		Document document = null;
		String webPage = "http://misiontokyo.com/";
		try {
			document = Jsoup.connect(webPage).get();
		}catch(ConnectException e) {
			
			JOptionPane.showMessageDialog(null,"No tiene internet","Asegúrese de que tiene conexión", 0);			
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "ERROR AL BUSCAR LAS NOTICIAS","ERROR" , 0);
		}
		Elements palabra2 = document.getElementsByClass("entry-title").get(0).select("a");
		cadena = palabra2.html();
		return cadena;
	}                                         	// NOTICIA ANIME2 republica.pe
	public static String anime2() {
		String cadena = null;
		Document document = null;
		String webPage = "https://larepublica.pe/animes/";
		try {
			document = Jsoup.connect(webPage).get();
		}catch(ConnectException e) {
			
			JOptionPane.showMessageDialog(null,"No tiene internet","Asegúrese de que tiene conexión", 0);			
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "ERROR AL BUSCAR LAS NOTICIAS","ERROR" , 0);
		}
		Elements palabra2 = document.getElementsByClass("CardSectionSubtitle CardSectionSubtitleLeft").get(1)
				.select("a");
		cadena = palabra2.html();
		return cadena;
	}                                         // NOTICIA ANIME3 ramenpara2
	public static String anime3() {
		String cadena = null;
		Document document = null;
		String webPage = "https://ramenparados.com/category/noticias/anime/";	
		try {
			document = Jsoup.connect(webPage).get();
		}catch(ConnectException e) {
			
			JOptionPane.showMessageDialog(null,"No tiene internet","Asegúrese de que tiene conexión", 0);			
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "ERROR AL BUSCAR LAS NOTICIAS","ERROR" , 0);
		}
		Elements palabra2 = document.getElementsByClass("widget-full-list-text left relative").get(3).select("a");
		cadena = palabra2.html();
		return cadena;
	}	

	public static void mandarCorreo() {		
		
		File f3 = new File("ConfiguracionEmail.txt");
		String correo="";
		String password="";
		String linea;
		String parteLinea[];
		String CorreoAdmin="";
		String linea2;
		String parteCorreoAdmin[];
		try {
		FileReader fr = new FileReader(f3);             //Leo el fichero de noticias y de usuarios
		BufferedReader br = new BufferedReader(fr);
		File f2 = new File("Usuarios.txt");
		FileReader fr2 = new FileReader(f2);
		BufferedReader br2 = new BufferedReader(fr2);
		
		linea2=br2.readLine();
		
		linea = br.readLine();
		                                                 //Saco el correo del administrador para enviarle las noticias
		parteCorreoAdmin = linea2.split(";");
		
		
		
		parteLinea = linea.split(";");
	
		br.close();
		br2.close();
		 correo= parteLinea[0];
		 password = parteLinea[1];
		 CorreoAdmin = parteCorreoAdmin[2];
		}catch(NullPointerException e) {
			JOptionPane.showMessageDialog(null, "ERROR EN LOS FICHEROS","ERROR" , 0);
		}catch(IOException e) {
			JOptionPane.showMessageDialog(null, "ERROR EN LOS FICHEROS","ERROR" , 0);
		}
		 		 
		DateFormat dateFormat = new SimpleDateFormat("HH:mm");  
		Date fechaHoy = new Date(); 		
		//Para validar que un correo existe---
		Pattern pattern = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
		Matcher mather = pattern.matcher(correo);
						
	    Properties props = new Properties();
			    	    
		props.put("mail.smtp.host","smtp.gmail.com");
		props.put("mail.smtp.port","587");
		props.put("mail.smtp.starttls.enable","true");
		props.put("mail.smtp.auth","true");
		props.put("mail.user", correo);
		props.put("mail.password",password);
		props.put("mail.smtp.ssl.trust", "*");
		Session session = Session.getInstance(props);

		      						
		int aviso=0;		
		try {			
			MimeMessage mimeMessage = new MimeMessage(session);
			
			mimeMessage.setFrom(new InternetAddress(correo,"JOSE LUIS"));									
			mimeMessage.addRecipients(Message.RecipientType.CC, CorreoAdmin );
						
			mimeMessage.setText("+DEPORTES:\n "+"-El Mundo: "+ deporte1()+"\n"  +"-El País: " +deporte2()+"\n"+"-Marca: "+deporte3()+"\n"+"+ARQUEOLOGÍA:\n "+"-El País: "+arqueologia1()+"\n"+"-ABC: "+arqueologia2()+"\n"+"-20 Minutos :"+arqueologia3()+"\n"+"+ANIME:\n "+"-Mision Tokyo: "+anime1()+"\n"+"-República: "+anime2()+"\n"+"-Ramen Para 2: "+anime3()+"\n"+dateFormat.format(fechaHoy));
			mimeMessage.setSubject("BREAKING NEWS");
			
			    	if(!mather.find()) {
					JOptionPane.showMessageDialog(null,"Correo no válido, acuérdese de añadir su nombre delante de @gmail.com","Introduzca un correo válido", 0);
				}else{					
			Transport transport = session.getTransport("smtp");
			transport.connect(correo, password);
			transport.sendMessage(mimeMessage, mimeMessage.getAllRecipients());
			//s.close();
			transport.close();
				}
		}catch(AddressException ex) {			
			JOptionPane.showMessageDialog(null, "ERROR AL ENVIAR EL CORREO","ERROR" , 0);
			aviso=1;
		}catch(MessagingException ex) {
			JOptionPane.showMessageDialog(null, "ERROR AL ENVIAR EL CORREO","ERROR" , 0);
			aviso=1;		
		}catch(IOException e) {
			JOptionPane.showMessageDialog(null, "ERROR AL ENVIAR EL CORREO","ERROR" , 0);	
		}			
		if(aviso==0) {
			JOptionPane.showMessageDialog(null,"Correo enviado correctamente");
		}	
	}			
	public static void mandarCorreoAutomatico() {						
		DateFormat dateFormat = new SimpleDateFormat("HH:mm");  
		Date fechaHoy = new Date(); 			
		int aviso=0;		
		File f3 = new File("ConfiguracionEmail.txt");
		String correo="";
		String password="";			
		String noticias="";
		try {
		FileReader fr = new FileReader(f3);    //Leo el fichero de configuración para coger el email y la pssw
		BufferedReader br = new BufferedReader(fr);		
		String linea;
		String parteLinea[];
		linea = br.readLine();		
		parteLinea = linea.split(";");	
		br.close();	 
		 correo= parteLinea[0];
		 password = parteLinea[1];	
		}catch(NullPointerException e) {
			JOptionPane.showMessageDialog(null, "ERROR EN LOS FICHEROS","ERROR" , 0);
		}catch(IOException e) {
			JOptionPane.showMessageDialog(null, "ERROR EN LOS FICHEROS","ERROR" , 0);
		}		
		File f1 = new File("Noticias.txt");		  			
			try {	
				FileReader fr = new FileReader(f1);    //Leo el fichero de noticias para mandar sus noticias
				BufferedReader br = new BufferedReader(fr);				
				String linea;
				String parteLinea[];			
				linea=br.readLine();
				while(linea!=null) {
		     	parteLinea= linea.split(";");
		     	
			    Properties props = new Properties();   //Propiedades para el envio del correo
				
				props.put("mail.smtp.host","smtp.gmail.com");
				props.put("mail.smtp.port","587");
				props.put("mail.smtp.starttls.enable","true");
				props.put("mail.smtp.auth","true");
				props.put("mail.user", correo);               
				props.put("mail.password",password);
				props.put("mail.smtp.ssl.trust", "*");
				Session session = Session.getInstance(props);
								
				MimeMessage mimeMessage = new MimeMessage(session);
				
				mimeMessage.setFrom(new InternetAddress(correo,"JOSE LUIS"));											
				mimeMessage.addRecipients(Message.RecipientType.CC, parteLinea[2] );	
				
				noticias += "+DEPORTES: \n";                           //Para saber que noticias tiene cada usuario
				if(linea.contains("marca")) 
					noticias+="-Marca: " +Funciones.deporte3()+"\n ";
				if(linea.contains("el PaísDepor"))
				    noticias+="-El País: "+Funciones.deporte2()+"\n ";
				if(linea.contains("el mundo"))
					noticias+="-El Mundo: "+Funciones.deporte1()+"\n"; 
			noticias+= "+ARQUELOGÍA: \n ";
				if(linea.contains("el país")) 
					noticias+= "-El País: "+Funciones.arqueologia1()+" \n ";
				if(linea.contains("abc"))
				    noticias+="-ABC: "+Funciones.arqueologia2()+" \n ";
				if(linea.contains("20 minutos"))
					noticias+="20 Minutos: "+Funciones.arqueologia3()+"\n";
			noticias+="+ANIME: \n";
				if(linea.contains("misiontokyo")) 
					noticias+="-Mision Tokyo: "+Funciones.anime1()+" \n ";
				if(linea.contains("ramen para 2"))
				    noticias+="-Ramen Para 2: "+Funciones.anime3()+" \n ";
				if(linea.contains("republica"))
					noticias+="-República: "+Funciones.anime2()+"\n";
			
				noticias+= dateFormat.format(fechaHoy);
												
				mimeMessage.setText(noticias);
				mimeMessage.setSubject("BREAKING NEWS");
							                                                    //El envío del mensaje
				Transport transport = session.getTransport("smtp");
				transport.connect(correo, password);
				transport.sendMessage(mimeMessage, mimeMessage.getAllRecipients());
				
				transport.close();
				noticias="";
				linea = br.readLine();						
				}
				br.close();
			   }catch(NullPointerException e) {
				   JOptionPane.showMessageDialog(null, "ERROR EN LOS FICHEROS","ERROR" , 0);
			   }catch(AddressException ex) {
				   JOptionPane.showMessageDialog(null, "ERROR AL ENVIAR EL CORREO","ERROR" , 0);
				aviso=1;
			     }catch(MessagingException ex) {
			    	 JOptionPane.showMessageDialog(null, "ERROR AL ENVIAR EL CORREO","ERROR" , 0);
				aviso=1;
			     }catch(Exception ex) {
			    	 JOptionPane.showMessageDialog(null, "ERROR AL ENVIAR EL CORREO","ERROR" , 0);
				aviso=1;
			    }
			if(aviso==0) {
				JOptionPane.showMessageDialog(null, "Correo enviado correctamente", "Correcto :)", 1);
			}
		   
		}
	public static boolean comprobarConexion() {		
		try {
			URL link = new URL("https://www.gmail.com/");  //Le paso el link de gmail para que compruebe si hay conexion
			URLConnection conexionGmail = link.openConnection();			
			conexionGmail.connect(); //para conectarse a gmail
			conexionGmail.getInputStream().close();
			return true;                 //Si comprueba que hay conexión en gmail devuelve true
		}catch(IOException e) {
			JOptionPane.showMessageDialog(null, "Ha habido un error de conexión","Error", 0);			
		}		
		return false;  //Si no hay internet devuelve false
	}
}
